import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

import { ProductsContextProvider } from './context/ProductsContext';
import { UsersContextProvider } from './context/UsersContext';
import { TraysContextProvider } from './context/TraysContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ProductsContextProvider>
      <UsersContextProvider>
        <TraysContextProvider>
          <App />
        </TraysContextProvider>
      </UsersContextProvider>
    </ProductsContextProvider>
  </React.StrictMode>
);
