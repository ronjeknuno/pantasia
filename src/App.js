import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { useEffect } from 'react';

import { useUsersContext } from './hooks/useUsersContext';

import 'bootstrap/dist/css/bootstrap.min.css';

// pages
import Home from './pages/Home';
import AdminPage from './pages/AdminPage';
import UserHomePage from './pages/UserHomePage';
import UserProductPage from './pages/UserProductPage';
import Page404 from './pages/Page404';
import UserTrayPage from './pages/UserTrayPage';

function App() {
  const { users, dispatch } = useUsersContext();

  useEffect(() => {
    const fetchUsers = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_API_URI}/users/profile`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
          },
        }
      );
      const json = await response.json();
      if (response.ok) {
        dispatch({ type: 'SET_USERS', payload: json });
      }
    };
    fetchUsers();
  }, []);
  console.log(users);

  return (
    <div className="App">
      <Router>
        <div className="pages">
          <Routes>
            {users && users.isAdmin ? (
              <>
                <Route path="*" element={<Page404 />} />
                <Route path="/" element={<Home />}></Route>
                <Route path="/admin" element={<AdminPage />} />
                <Route path="/home" element={<UserHomePage />} />
                <Route path="/shop" element={<UserProductPage />} />
              </>
            ) : (
              <>
                <Route path="/home" element={<UserHomePage />} />
                <Route path="/shop" element={<UserProductPage />} />
                <Route path="/tray" element={<UserTrayPage />} />
              </>
            )}
            <Route path="/" element={<Home />}></Route>
            <Route path="*" element={<Page404 />} />
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
