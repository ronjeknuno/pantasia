//components
import NavBar from '../components/NavBar';
import Landing from '../components/Landing';
import FeaturedContainer from '../components/FeaturedContainer';
import Footer from '../components/Footer';

function UserHomePage() {
  return (
    <div>
      <NavBar />
      <Landing />
      <FeaturedContainer />
      <Footer />
    </div>
  );
}

export default UserHomePage;
