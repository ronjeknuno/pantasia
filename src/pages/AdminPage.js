import { useEffect } from 'react';
import { useProductsContext } from '../hooks/useProductsContext';
import { Container } from 'react-bootstrap';

// components
import NavBar from '../components/NavBar';
import ProductTable from '../components/Admin/ProductTable';
import SideBarMenu from '../components/Admin/SideBarMenu';

function AdminPage() {
  const { products, dispatch } = useProductsContext();

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_API_URI}/products/allProducts`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
          },
        }
      );
      const json = await response.json();
      if (response.ok) {
        dispatch({ type: 'SET_PRODUCTS', payload: json });
      }
    };
    fetchProducts();
  }, []);
  // console.log(products);

  return (
    <>
      <Container fluid>
        <NavBar />
        {/* <SideBarMenu /> */}
        {products && <ProductTable products={products} />}
      </Container>
    </>
  );
}

export default AdminPage;
