import {
  Container,
  Col,
  Card,
  Form,
  Button,
  Row,
  Image,
} from 'react-bootstrap';
import './css/NavBar.css';

import ViewProductModal from './Modals/ViewProductModal';
import CheckedOutModal from './Modals/CheckOutModal';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPesoSign } from '@fortawesome/free-solid-svg-icons';

function ProductContainer({ products }) {
  console.log(products);
  return (
    <Container>
      <Row>
        {products.map((product) => (
          <Col
            key={product._id}
            xs={12}
            md={6}
            lg={4}
            className="mb-5 text-center d-flex justify-content-center"
          >
            <Card className="text-center mt-4 col-12 h-100">
              <Card.Header className="fs-2">{product.name}</Card.Header>
              <Card.Body>
                <Card.Title>
                  <Image
                    thumbnail
                    src={product.image.url}
                    className="col-10 col-md-8 col-lg-12"
                  />
                </Card.Title>
                <Card.Text className="">{product.description}</Card.Text>
                <Card.Text className="fw-bolder">
                  Stocks : {product.stocks}pcs
                </Card.Text>
                <Card.Text className="text-capitalize fw-bolder ">
                  Price : <FontAwesomeIcon icon={faPesoSign}></FontAwesomeIcon>
                  {product.price}
                </Card.Text>
              </Card.Body>
              <Card.Footer className="d-flex justify-content-around">
                <ViewProductModal productID={product} />
                <CheckedOutModal productID={product} />
              </Card.Footer>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
}

export default ProductContainer;
