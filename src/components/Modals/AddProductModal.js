import React, { useState } from 'react';
import { Form, Button, Modal, Spinner } from 'react-bootstrap';
import Swal from 'sweetalert2';

import { useProductsContext } from '../../hooks/useProductsContext';

function AddProductModal() {
  const { dispatch } = useProductsContext();
  const [show, setShow] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [stocks, setStocks] = useState(0);

  const [image, setImage] = useState('');
  const [selectedImage, setSelectedImage] = useState('');
  const [previewSource, setPreviewSource] = useState('');
  const [loading, setLoading] = useState(false);

  //   const history = useNavigate();

  const handleClose = () => {
    setLoading(false);
    setShow(false);
  };
  const handleShow = () => setShow(true);

  const imageHandler = (event) => {
    console.log('submitting');
    const file = event.target.files[0];
    previewfile(file);
  };

  const previewfile = async (file) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setPreviewSource(reader.result);
    };
  };

  const uploadImage = async (base64EncodedImage) => {
    console.log(base64EncodedImage);
    setSelectedImage(base64EncodedImage);
  };

  const addProductHandler = async (event) => {
    event.preventDefault();
    if (!previewSource) return;
    const reader = new FileReader();
    uploadImage(previewSource);
    setLoading(true);
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URI}/products/addProduct`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
          },
          body: JSON.stringify({
            name,
            description,
            price,
            stocks,
            data: selectedImage,
          }),
        }
      );
      const json = await response.json();
      if (response.ok) {
        Swal.fire({
          title: 'Successfully Added!',
          icon: 'success',
          text: json.message,
        });
        setName('');
        setDescription('');
        setPrice('');
        setStocks('');
        setSelectedImage(null);
        setShow(false);
        setLoading(false);
        setPreviewSource('');
        dispatch({ type: 'CREATE_PRODUCTS', payload: json.result });
      }
    } catch (error) {
      Swal.fire({
        title: 'Error Occured',
        icon: 'error',
        text: 'Please Try Again Later!',
      });
      setLoading(false);
    }
  };

  return (
    <>
      <Button
        variant="outline-light"
        className="text-dark"
        onClick={handleShow}
      >
        +
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            onSubmit={addProductHandler}
            className="col-12 px-3 was-invalidated "
          >
            <Form.Group className="">
              <Form.Label className="col-12 d-flex flex-column align-items-center">
                BreadImage
                {previewSource && (
                  <img
                    src={previewSource}
                    alt="Chosen"
                    className="col-4 pb-3"
                  />
                )}
                <Form.Control
                  type="file"
                  accept="image/"
                  name="image"
                  onChange={imageHandler}
                />
              </Form.Label>
            </Form.Group>

            <Form.Group className="">
              <Form.Label className="col-12">
                Bread Name
                <Form.Control
                  type="text"
                  placeholder="Name the Bread"
                  value={name}
                  onChange={(event) => setName(event.target.value)}
                  required
                />
              </Form.Label>
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>Bread Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Describe the kind of Bread"
                value={description}
                onChange={(event) => setDescription(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-5 d-flex flex-row align-items-center justify-content-between">
              <Form.Label className="col-5">
                Price
                <Form.Control
                  type="number"
                  placeholder="Enter Last Name"
                  value={price}
                  onChange={(event) => setPrice(event.target.value)}
                  required
                />
              </Form.Label>

              <Form.Label className="col-5">
                Stocks
                <Form.Control
                  type="number"
                  placeholder="Enter Last Name"
                  value={stocks}
                  onChange={(event) => setStocks(event.target.value)}
                  required
                />
              </Form.Label>
            </Form.Group>

            <Modal.Footer className="">
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              {!loading ? (
                <Button variant="primary" type="submit">
                  Add Product
                </Button>
              ) : (
                <Button variant="primary" type="submit" disabled>
                  <Spinner
                    as="span"
                    animation="grow"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                  Adding {name}...
                </Button>
              )}
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default AddProductModal;
