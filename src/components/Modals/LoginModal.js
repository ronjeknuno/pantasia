import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Form, Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useUsersContext } from '../../hooks/useUsersContext';
import RegisterModal from './RegisterModal';

function LoginModal() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disable, setDisable] = useState(true);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const history = useNavigate();
  const { users, dispatch } = useUsersContext();
  console.log(users);

  useEffect(() => {
    if (email && password) {
      setDisable(false);
    } else {
      setDisable(true);
    }
  }, [email, password]);

  const login = (event) => {
    event.preventDefault();
    const loginUser = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_API_URI}/users/login`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email,
            password,
          }),
        }
      );
      const json = await response.json();
      if (response.ok) {
        // console.log(json);
        if (json.accessToken !== 'empty') {
          localStorage.setItem('accessToken', json.accessToken);
          retreiveUserDetails(json.accessToken);
          console.log(json);
          console.log(json.isAdmin);
          if (json.isAdmin) {
            Swal.fire({
              title: '<strong>Wild Admin Appear!</strong>',
              html: '<i>Welcome to Admin dashboard</i>',
              icon: 'success',
            });
            history('/admin');
          } else {
            handleClose();
            Swal.fire({
              title: '<strong>Login Successfully!</strong>',
              html: '<i>Welcome to Homepage</i>',
              icon: 'success',
            });
            history('/home');
          }
        } else {
          // console.log(`Wrong Credentials`);
          Swal.fire({
            title: '<strong>Login Failed!</strong>',
            html: '<i>Try Again!</i>',
            icon: 'error',
          });
          setPassword('');
        }
      }
    };
    loginUser();

    const retreiveUserDetails = (token) => {
      fetch(`${process.env.REACT_APP_API_URI}/users/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          dispatch({ type: 'SET_USERS', payload: data });
        });
    };
  };

  return (
    <>
      <p onClick={handleShow}>Login </p>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form className="col-12">
            <Form.Group className="" controlId="formEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="" controlId="formPassword">
              <Form.Label>Enter Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mt-3" controlId="formCheckbox"></Form.Group>
            <div className="d-flex mb-3 text-muted">
              <span>No Account as of the moment? </span>
              <span>
                <strong>
                  <RegisterModal />
                </strong>
              </span>
            </div>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button
                variant="primary"
                type="submit"
                onClick={login}
                disabled={disable}
              >
                Login
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}
export default LoginModal;
