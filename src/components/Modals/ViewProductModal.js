import React, { useState, useEffect, useContext } from 'react';
import { Card, Form, Button, Modal } from 'react-bootstrap';

function ViewProductModal({ productID }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button onClick={handleShow} variant="info">
        View
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Product Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            // onSubmit={editProductHandler}
            className="col-12 px-3 was-invalidated "
          >
            <Card className="text-center">
              <Card.Header></Card.Header>
              <Card.Body>
                <Card.Title>{productID.name}</Card.Title>
                <Card.Text>{productID.description}</Card.Text>
                <Card.Text>&#8369; {productID.price}</Card.Text>
              </Card.Body>
              <Card.Footer className="text-muted">Bread, Oat, Pan</Card.Footer>
            </Card>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default ViewProductModal;
