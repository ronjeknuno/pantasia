import React, { useState, useEffect, useContext } from 'react';
import {
  Form,
  Card,
  Button,
  Container,
  Row,
  Col,
  Modal,
} from 'react-bootstrap';
import Swal from 'sweetalert2';

import { useUsersContext } from '../../hooks/useUsersContext';

function CheckedOutModal({ productID }) {
  const [show, setShow] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [total, setTotal] = useState(productID.price);
  const { users } = useUsersContext();

  useEffect(() => {
    setTotal(quantity * productID.price);
  }, [quantity]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const LoginShow = () => {
    Swal.fire({
      title: 'Access Fail',
      icon: 'error',
      text: 'Must Login First!',
    });
  };

  const checkOut = async (event) => {
    event.preventDefault();

    if (users.isAdmin) {
      Swal.fire({
        title: 'Access Error',
        icon: 'error',
        text: 'Admins are not allowed to checkout',
      });
    } else {
      if (quantity <= 0) {
        Swal.fire({
          title: 'Checkout Fail',
          icon: 'error',
          text: `Invalid Checkout!`,
        });
        setShow(false);
        setQuantity(1);
      } else {
        const response = await fetch(
          `${process.env.REACT_APP_API_URI}/trays/${productID._id}`,
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
            },
            body: JSON.stringify({
              quantity,
            }),
          }
        );
        const json = await response.json();
        if (response.ok) {
          Swal.fire({
            title: 'Checkout Successfully',
            icon: 'success',
            text: `${quantity} ${productID.name}: ₱${total}!`,
          });
          console.log(json);
          setShow(false);
          setQuantity(1);
        }
      }
    }
  };

  return (
    <>
      <Button onClick={users ? handleShow : LoginShow} variant="secondary">
        CheckOut
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title className="text-center">Check Out Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            // onSubmit={editProductHandler}
            className="col-12 px-3 was-invalidated "
          >
            <Card>
              <Card.Header as="h5">{productID.name}</Card.Header>
              <Card.Body>
                <Card.Title>{productID.description}</Card.Title>
                <Card.Text>
                  <Card.Text>
                    Price: <strong>&#8369; {productID.price}.00</strong>
                  </Card.Text>
                </Card.Text>
                Quantity:
                <input
                  className="col-2"
                  type={'number'}
                  value={quantity}
                  onChange={(event) => setQuantity(event.target.value)}
                  required
                ></input>
                <Card.Text className="mt-3">
                  Total: <strong>&#8369; {total}.00</strong>
                </Card.Text>
              </Card.Body>
            </Card>

            <Modal.Footer className="">
              <Button variant="secondary" onClick={handleClose}>
                Cancel
              </Button>
              <Button variant="primary" type="submit" onClick={checkOut}>
                CheckOut
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default CheckedOutModal;
