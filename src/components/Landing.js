import './css/Landing.css';
import Carousel from 'react-bootstrap/Carousel';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
function Landing() {
  return (
    <>
      <div className="landing shapedividers_com-350">
        <Container
          fluid
          className="d-flex 
        justify-content-center"
        >
          <Carousel variant="dark" fade controls={false}>
            <Carousel.Item className="mb-5 mb-xl-0">
              <img
                className="d-block mx-auto landing-image"
                src={require('../assets/cover/landing.jpg')}
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item className="mb-5 mb-xl-0">
              <img
                className="d-block mx-auto landing-image "
                src={require('../assets/cover/landing2.jpg')}
                alt="Second slide"
              />
            </Carousel.Item>
          </Carousel>
          <div className="position-absolute col-6 col-md-5  text-white text-center landing-text">
            <h1>
              <strong>Baked with love from our kitchen to yours</strong>
            </h1>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni
              maxime ratione deleniti sequi in dolorum officia, expedita earum
              qui, quaerat quasi adipisci tempora est cum aut et. Aliquid,
              fugiat officia.
            </p>
            <Button as={Link} to="/shop" className="try-btn">
              TRY OUR BREADS
            </Button>
          </div>
        </Container>
      </div>
    </>
  );
}

export default Landing;
