import Carousel from 'react-bootstrap/Carousel';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

function Jumbotron() {
  return (
    <div>
      <div class="jumbotron">
        <Carousel variant="dark" controls={false}>
          <Carousel.Item className="mb-5 mb-xl-0">
            <img
              className="d-block mx-auto landing-image blur"
              src={require('../assets/cover/landing.jpg')}
              alt="First slide"
            />
          </Carousel.Item>
        </Carousel>
        <div className="position-absolute jumboltron-text text-white">
          <h1>
            <Link to={'/home'}>Home</Link>/<Link to={'/shop'}>Shop</Link>
          </h1>
        </div>
      </div>
    </div>
  );
}

export default Jumbotron;
