import {
  Table,
  Container,
  Col,
  Row,
  Image,
  Card,
  Button,
} from 'react-bootstrap';
import AddProductModal from '../Modals/AddProductModal';
import EditProductModal from '../Modals/EditProductModal';
import ProductStatus from './ProductStatus';
import RemoveProduct from './RemoveProduct';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPesoSign } from '@fortawesome/free-solid-svg-icons';

function ProductTable({ products }) {
  console.log(products);

  return (
    <Container fluid="xs">
      <Row className="d-flex justify-content-center">
        <Col md={12} lg={12} xl={10}>
          <Table
            striped
            hover
            className="mt-5 pt-4 col-4 text-center d-none d-md-block"
          >
            <thead>
              <tr className="text-center">
                <th className="d-flex justify-content-center align-items-center">
                  <span>Product Image</span>
                  <span>
                    <AddProductModal />
                  </span>
                </th>
                <th className="col-1 col-xs-2 col-md-1 col-lg-2">
                  Product Name
                </th>
                <th className="col-4">Description</th>
                <th className="col">Price</th>
                <th>Stocks</th>
                <th>Status</th>
                <th className="col-2">Action</th>
              </tr>
            </thead>
            <tbody>
              {products.map((product) => (
                <tr key={product._id}>
                  <td className="col-3 ">
                    <Image
                      fluid
                      thumbnail
                      src={product.image.url}
                      className="col-6"
                    />
                  </td>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>
                    <FontAwesomeIcon icon={faPesoSign}></FontAwesomeIcon>
                    {product.price}
                  </td>
                  <td>{product.stocks}</td>
                  <td>
                    {product.isActive && product.stocks > 0
                      ? 'Active'
                      : 'Inactive'}
                  </td>
                  <td className="col-md-12 col-lg-12 col-xl-4">
                    <EditProductModal productID={product} />

                    <ProductStatus productID={product} />
                    <RemoveProduct productID={product} />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>

          <Container fluid="xs" className="my-5">
            {products.map((product) => (
              <div className=" d-flex justify-content-center d-md-none">
                <Card className="text-center mt-4 col-11">
                  <Card.Header className="fs-1">{product.name}</Card.Header>
                  <Card.Body>
                    <Card.Title>
                      <Image
                        fluid
                        thumbnail
                        src={product.image.url}
                        className="col-7 col-md-4"
                      />
                    </Card.Title>
                    <Card.Text className="">{product.description}</Card.Text>
                    <Card.Text className="fw-bolder">
                      Stocks :{product.stocks}pcs
                    </Card.Text>
                    <Card.Text className="text-capitalize fw-bolder ">
                      Price:
                      <FontAwesomeIcon icon={faPesoSign}></FontAwesomeIcon>
                      {product.price}
                    </Card.Text>
                    <Card.Text className="fw-bolder">
                      Status :
                      {product.isActive && product.stocks > 0
                        ? ' Active'
                        : ' Inactive'}
                    </Card.Text>
                  </Card.Body>
                  <Card.Footer>
                    <EditProductModal productID={product} />

                    <ProductStatus productID={product} />
                    <RemoveProduct productID={product} />
                  </Card.Footer>
                </Card>
              </div>
            ))}
          </Container>
        </Col>
      </Row>
    </Container>
  );
}

export default ProductTable;
