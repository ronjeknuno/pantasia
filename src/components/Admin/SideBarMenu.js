import React from 'react';
import { Link } from 'react-router-dom';

function SideBarMenu() {
  /* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
  function openNav(event) {
    event.preventDefault();
    document.getElementById('mySidenav').style.width = '250px';
    document.getElementById('main').style.marginLeft = '250px';
  }

  /* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
  function closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    document.getElementById('main').style.marginLeft = '0';
  }
  return (
    <>
      <div>
        <div id="main">
          <Link onClick={openNav}>&#9776; open</Link>
        </div>
        <div id="mySidenav" class="sidenav">
          <Link className="closebtn" onClick="closeNav()">
            &times;
          </Link>
          <Link>About</Link>
          <Link>About</Link>
          <Link>About</Link>
          <Link>About</Link>
        </div>

        <span onclick="openNav()">open</span>
      </div>
    </>
  );
}

export default SideBarMenu;
