import React from 'react';
import { Form, Button, Container, Row, Col, Modal } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { useProductsContext } from '../../hooks/useProductsContext';

import Swal from 'sweetalert2';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashCan } from '@fortawesome/free-solid-svg-icons';

function RemoveProduct({ productID }) {
  //   console.log(productID);
  const { dispatch } = useProductsContext();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const confirmationHandler = (event) => {
    event.preventDefault();
    handleShow();
  };

  const removeProductHandler = async (event) => {
    event.preventDefault();

    const response = await fetch(
      `${process.env.REACT_APP_API_URI}/products/allProducts/${productID._id}`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
        },
      }
    );
    const json = await response.json();
    if (response.ok) {
      Swal.fire({
        title: 'Product Deleted!',
        icon: 'warning',
        text: json.message,
      });
      setShow(false);
    }
  };

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_API_URI}/products/allProducts`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
          },
        }
      );
      const json = await response.json();
      if (response.ok) {
        dispatch({ type: 'SET_PRODUCTS', payload: json });
      }
    };
    fetchProducts();
  }, [show]);
  return (
    <>
      <Button onClick={confirmationHandler} variant="danger">
        <FontAwesomeIcon icon={faTrashCan}></FontAwesomeIcon>
      </Button>

      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are You Sure to DELETE {productID.name} ?</Modal.Body>
        <Modal.Footer className="">
          <Button variant="success" onClick={handleClose}>
            Close
          </Button>
          <Button variant="danger" onClick={removeProductHandler} type="submit">
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default RemoveProduct;
